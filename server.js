const express = require('express');

const PORT = process.env.PORT || 4000

let app = express();

app.get('/',(req,res) => {
    res.send('Welcome');
})

app.listen(PORT,() => {
    console.log(`server is listening on port ${PORT}`);
})